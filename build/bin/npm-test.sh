#!/bin/bash

# Old jQuery versions
./node_modules/.bin/bower install jquery#1.7.2
./node_modules/.bin/grunt test
./node_modules/.bin/bower install jquery#1.8.3
./node_modules/.bin/grunt test
# Modern jQuery versions which require jquery-migrate plugin to work
./node_modules/.bin/bower install jquery-migrate#1.2.1
./node_modules/.bin/bower install jquery#1.10.1
./node_modules/.bin/grunt test
./node_modules/.bin/bower install jquery-migrate#1.2.1
./node_modules/.bin/bower install jquery#2.1.0
./node_modules/.bin/grunt test
