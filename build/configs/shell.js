module.exports = function (grunt) {
    return {
        flatpackI18n: {
            options: {
                stdout: true
            },
            command: 'build/bin/propertiesToJs.sh en',
        }
    };
};
