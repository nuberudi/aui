(function() {

    if (window.CustomEvent) {
        return;
    }

    function CustomEvent (event, params) {
        params = params || {};

        var evt = document.createEvent('CustomEvent');

        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);

        return evt;
    };

    CustomEvent.prototype = window.Event.prototype;

    window.CustomEvent = CustomEvent;

}());
