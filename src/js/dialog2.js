(function($) {
    //Bind an event to wrap around the dialog's tabbable elements
    (function(){
        $(document).on('focusout', '.aui-dialog2',  function(event) {
            var $topMostDialog = AJS.LayerManager.global.getTopLayer();
            if (!$topMostDialog) {
                return;
            }

            var focusFrom = event.target;
            var focusTo = event.relatedTarget;

            AJS.dim.$dim.attr('tabIndex', 0); //required, or the last element's focusout event will go to the browser

            if ($topMostDialog.has(focusTo).length > 0) {
                return;
            }

            var $tabbableElements = $topMostDialog.find(':aui-tabbable');
            var $firstTabbableElement = AJS.$($tabbableElements.first());
            var $lastTabbableElement = AJS.$($tabbableElements.last());

            if ($firstTabbableElement.is(focusFrom)) {
                $lastTabbableElement.focus();
            } else if ($lastTabbableElement.is(focusFrom)) {
                $firstTabbableElement.focus();
            }
        });
    })();

    var defaults = {
        "aui-focus": "false", // do not focus by default as it's overridden below
        "aui-blanketed": "true"
    };

    function applyDefaults($el) {
        jQuery.each(defaults, function(key, value) {
            var dataKey = "data-" + key;
            if (!$el[0].hasAttribute(dataKey)) {
                $el.attr(dataKey, value);
            }
        });
    }

    function Dialog2(selector) {
        if (selector) {
            this.$el = $(selector);
        }
        else {
            this.$el = $(AJS.parseHtml($(aui.dialog.dialog2({}))));
        }
        applyDefaults(this.$el);
    }

    Dialog2.prototype.on = function(event, fn) {
        AJS.layer(this.$el).on(event, fn);
        return this;
    };

    Dialog2.prototype.off = function(event, fn) {
        AJS.layer(this.$el).off(event, fn);
        return this;
    };

    Dialog2.prototype.show = function() {
        var layer = AJS.layer(this.$el);
        layer.show();
        return this;
    };

    Dialog2.prototype.hide = function() {
        AJS.layer(this.$el).hide();
        return this;
    };

    Dialog2.prototype.remove = function() {
        AJS.layer(this.$el).remove();
        return this;
    };

    AJS.dialog2 = AJS._internal.widget('dialog2', Dialog2);

    var GLOBAL_EVENT_PREFIX = "_aui-internal-dialog2-global-";

    function bindToLayerEvent(eventName) {
        AJS.layer.on(eventName, function(e, $el) {
            if ($el.is('.aui-dialog2')) {
                var dialogEvent = AJS.$.Event(GLOBAL_EVENT_PREFIX + eventName);
                $(document).trigger(dialogEvent, [$el]);
                return !dialogEvent.isDefaultPrevented();
            }
            else {
                return true;
            }
        });
    }
    // wrap layer events
    var LAYER_EVENTS = ['show', 'hide', 'beforeShow', 'beforeHide'];
    for (var i = 0; i < LAYER_EVENTS.length; ++i) {
        bindToLayerEvent(LAYER_EVENTS[i]);
    }

    AJS.dialog2.on = function(eventName, fn) {
        $(document).on(GLOBAL_EVENT_PREFIX + eventName, fn);
        return this;
    };

    AJS.dialog2.off = function(eventName, fn) {
        $(document).off(GLOBAL_EVENT_PREFIX + eventName, fn);
        return this;
    };

    /* Live events */

    $(document).on('click', '.aui-dialog2-header-close', function(e) {
        e.preventDefault();
        AJS.dialog2($(this).closest('.aui-dialog2')).hide();
    });

    AJS.dialog2.on('show', function(e, $el) {
        var selectors = ['.aui-dialog2-content', '.aui-dialog2-footer', '.aui-dialog2-header'];
        var $selected;
        selectors.some(function(selector) {
            $selected = $el.find(selector + ' :aui-tabbable');
            return $selected.length;
        });
        $selected && $selected.first().focus();
    });

    AJS.dialog2.on('hide', function(e,$el) {
        var layer = AJS.layer($el);

        if ($el.data("aui-remove-on-hide")) {
            layer.remove();
        }
    });

})(AJS.$);
