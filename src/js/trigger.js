(function ($, skate) {
    'use strict';

    function findControlled(trigger) {
        return document.getElementById(trigger.getAttribute('aria-controls'));
    }

    function hide(e) {
        var controls = findControlled(e.target);
        var $controls = $(controls);

        if (!controls.isVisible()) {
            return;
        }

        controls.hide();
    }

    function show(e) {
        var controls = findControlled(e.target);
        var $controls = $(controls);

        if (controls.isVisible()) {
            return;
        }

        controls.show();
    }

    skate('[data-aui-trigger="toggle"]', function() {
        this.addEventListener('click', function(e) {
            return findControlled(this).isVisible() ? hide(e) : show(e);
        });
    });

}(jQuery, window.skate || require('skate')));
