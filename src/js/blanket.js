(function($) {
    
    var $overflowEl;

    /**
     *
     * Dims the screen using a blanket div
     * @param useShim deprecated, it is calculated by dim() now
     */
    AJS.dim = function (useShim, zIndex) {

        if  (!$overflowEl) {
            $overflowEl = $(document.body);
        }

        if (useShim === true) {
            AJS.log("DEPRECATED: useShim is calculated by dim() now");
        }

        if (!AJS.dim.$dim) {
            AJS.dim.$dim = AJS("div").addClass("aui-blanket");
            if (zIndex) {
                AJS.dim.$dim.css({zIndex: zIndex});
            }
            if ($.browser.msie) {
                AJS.dim.$dim.css({width: "200%", height: Math.max($(document).height(), $(window).height()) + "px"});
            }
            $("body").append(AJS.dim.$dim);

            // Add IFrame shim. IE9+ does not need a shim, but IE8 does
            if (useShim || (AJS.$.browser.msie && Math.floor(AJS.$.browser.version) < 9)) {
                //Tabbing onto the iframe caused a bug in IE9, so tabindex="-1"
                AJS.dim.$shim = $('<iframe frameBorder="0" class="aui-blanket-shim" tabindex="-1" src="about:blank"/>');
                AJS.dim.$shim.css({height: Math.max($(document).height(), $(window).height()) + "px"});
                if (zIndex) {
                    AJS.dim.$shim.css({zIndex: zIndex - 1});
                }
                $("body").append(AJS.dim.$shim);
                // AJS.dim.shim is a legacy alias to AJS.dim.$shim
                AJS.dim.shim = AJS.dim.$shim;
            }

            
            AJS.dim.cachedOverflow = $overflowEl.css("overflow");
            $overflowEl.css("overflow", "hidden");
        }

        return AJS.dim.$dim;
    };

    /**
     * Removes semitransparent DIV
     * @see AJS.dim
     */
    AJS.undim = function() {
        if (AJS.dim.$dim) {
            AJS.dim.$dim.remove();
            AJS.dim.$dim = null;

            if (AJS.dim.$shim) {
                AJS.dim.$shim.remove();
                AJS.dim.$shim = null;
            }

            $overflowEl && $overflowEl.css("overflow",  AJS.dim.cachedOverflow);

            // Safari bug workaround
            if ($.browser.safari) {
                var top = $(window).scrollTop();
                $(window).scrollTop(10 + 5 * (top == 10)).scrollTop(top);
            }
        }
    };

}(AJS.$));