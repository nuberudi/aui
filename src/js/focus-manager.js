AJS.FocusManager = (function($) {

    (function initSelectors() {
        /*
         :tabbable and :focusable functions from jQuery UI v 1.10.4
         renamed to :aui-tabbable and :aui-focusable to not clash with jquery-ui if it's included.
         Code modified slightly to be compatible with jQuery < 1.8 (.addBack() replaced with .andSelf())
         */
        function visible( element ) {
            return !$( element ).parents().andSelf().filter(function() {
                return $.curCSS( this, "visibility" ) === "hidden" ||
                    $.expr.filters.hidden( this );
            }).length;
        }

        function focusable( element, isTabIndexNotNaN ) {
            var nodeName = element.nodeName.toLowerCase();

            if (nodeName === 'area') {
                var map = element.parentNode;
                var mapName = map.name;
                var imageMap = $('img[usemap=#' + mapName + ']').get();

                if (!element.href || !mapName || map.nodeName.toLowerCase() !== 'map') {
                    return false;
                }
                return imageMap && visible(imageMap);
            }
            var isFormElement = /input|select|textarea|button|object/.test(nodeName);
            var isAnchor = nodeName === 'a';
            var isAnchorTabbable = (element.href || isTabIndexNotNaN);

            return (
                isFormElement ? !element.disabled :
                    (isAnchor ? isAnchorTabbable : isTabIndexNotNaN)
                ) && visible(element);
        }

        function tabbable( element ){
            var tabIndex = $.attr( element, "tabindex" ),
                isTabIndexNaN = isNaN( tabIndex );
            var hasTabIndex = ( isTabIndexNaN || tabIndex >= 0 );

            return hasTabIndex && focusable( element, !isTabIndexNaN );
        }

        $.extend( $.expr[ ":" ], {
            'aui-focusable': function( element ) {
                return focusable( element, !isNaN( $.attr( element, "tabindex" ) ) );
            },

            'aui-tabbable': tabbable
        });
    }());

    var RESTORE_FOCUS_DATA_KEY = "_aui-focus-restore";

    function FocusManager() {
    }
    FocusManager.defaultFocusSelector = ":aui-tabbable";
    FocusManager.prototype.enter = function($el) {
        // remember focus on old element
        $el.data(RESTORE_FOCUS_DATA_KEY, $(document.activeElement));

        // focus on new selector
        if ($el.attr("data-aui-focus") !== "false") {
            var focusSelector = $el.attr('data-aui-focus-selector') || FocusManager.defaultFocusSelector;
            var $focusEl = $el.is(focusSelector) ? $el : $el.find(focusSelector);
            $focusEl.first().focus();
        }
    };
    FocusManager.prototype.exit = function($el) {
        // AUI-1059: remove focus from the active element when dialog is hidden
        var activeElement = document.activeElement;
        if ($el[0] === activeElement || $el.has(activeElement).length) {
            $(activeElement).blur();
        }

        var $restoreFocus = $el.data(RESTORE_FOCUS_DATA_KEY);
        if ($restoreFocus && $restoreFocus.length) {
            $el.removeData(RESTORE_FOCUS_DATA_KEY);
            $restoreFocus.focus();
        }
    };

    FocusManager.global = new FocusManager();

    return FocusManager;
}(AJS.$));
