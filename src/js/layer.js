(function($) {

    var EVENT_PREFIX = "_aui-internal-layer-";
    var GLOBAL_EVENT_PREFIX = "_aui-internal-layer-global-";

    function Layer(selector) {
        this.$el = $(selector || '<div class="aui-layer" aria-hidden="true"></div>');
    }

    Layer.prototype.changeSize = function(width, height) {
        this.$el.css('width', width);
        this.$el.css('height', height === 'content' ? '' : height);
        return this;
    };

    Layer.prototype.on = function(event, fn) {
        this.$el.on(EVENT_PREFIX + event, fn);
        return this;
    };

    Layer.prototype.off = function(event, fn) {
        this.$el.off(EVENT_PREFIX + event, fn);
        return this;
    };

    Layer.prototype.show = function() {
        if (this.$el.is(':visible')) {
            return this;
        }
        var e = AJS.$.Event(EVENT_PREFIX + "beforeShow");
        this.$el.trigger(e);

        var e2 = AJS.$.Event(GLOBAL_EVENT_PREFIX + "beforeShow");
        $(document).trigger(e2,[this.$el]);
        if (!e.isDefaultPrevented() && !e2.isDefaultPrevented()) {
            AJS.LayerManager.global.push(this.$el);
        }
        return this;
    };

    Layer.prototype.hide = function() {
        if (!this.$el.is(':visible')) {
            return this;
        }
        var e = AJS.$.Event(EVENT_PREFIX + "beforeHide");
        this.$el.trigger(e);

        var e2 = AJS.$.Event(GLOBAL_EVENT_PREFIX + "beforeHide");
        $(document).trigger(e2,[this.$el]);
        if (!e.isDefaultPrevented() && !e2.isDefaultPrevented()) {
            AJS.LayerManager.global.popUntil(this.$el);
        }
        return this;
    };

    Layer.prototype.isVisible = function() {
        return this.$el.attr("aria-hidden") === "false";
    };

    Layer.prototype.remove = function() {
        this.hide();
        this.$el.remove();
        this.$el = null;
    };

    Layer.prototype._showLayer = function(zIndex) {
        if (!this.$el.parent().is('body')) {
            this.$el.appendTo(document.body);
        }
        // inverse order to hideLayer
        this.$el.data('_aui-layer-cached-z-index', this.$el.css('z-index'));
        this.$el.css("z-index", zIndex);
        this.$el.attr("aria-hidden", "false");
        AJS.FocusManager.global.enter(this.$el);
        this.$el.trigger(EVENT_PREFIX + "show");
        $(document).trigger(GLOBAL_EVENT_PREFIX + "show", [this.$el]);
    };

    Layer.prototype._hideLayer = function() {
        // inverse order to showLayer
        AJS.FocusManager.global.exit(this.$el);
        this.$el.attr("aria-hidden", "true");
        this.$el.css('z-index', this.$el.data('_aui-layer-cached-z-index') || '');
        this.$el.data('_aui-layer-cached-z-index', '');
        this.$el.trigger(EVENT_PREFIX + "hide");
        $(document).trigger(GLOBAL_EVENT_PREFIX + "hide", [this.$el]);
    };

    Layer.prototype.isBlanketed = function() {
        return this.$el.attr("data-aui-blanketed") === "true";
    };

    Layer.prototype.isModal = function() {
        return this.$el.attr("data-aui-modal") === "true";
    };

    AJS.layer = AJS._internal.widget('layer', Layer);

    AJS.layer.on = function(eventName, fn) {
        $(document).on(GLOBAL_EVENT_PREFIX + eventName, fn);
        return this;
    };

    AJS.layer.off = function(eventName, fn) {
        $(document).off(GLOBAL_EVENT_PREFIX + eventName, fn);
        return this;
    };

}(AJS.$));