define(['dialog2', 'soy/dialog2', 'aui-qunit'], function() {
    module("Dialog2 integration tests", {
        setup: function() {
            sinon.spy(AJS, "dim");
            sinon.spy(AJS, "undim");
        },
        teardown: function() {
            AJS.dim.restore();
            AJS.undim.restore();
            // AJS.layer.show() moves the element to be a child of the body, so clean up popups
            AJS.$(".aui-layer").remove();
        },
        pressEsc: function() {
            var e = jQuery.Event("keydown");
            e.keyCode = AJS.keyCode.ESCAPE;
            $(document).trigger(e);
        },
        createContentEl: function() {
            return AJS.$(aui.dialog.dialog2({
                content: "Hello world"
            })).appendTo("#qunit-fixture");
        },
        createInputContentEl: function () {
            return AJS.$(aui.dialog.dialog2({
                headerActionContent: "<button id='my-button'>Button</button>",
                content: "<input type='text' id='my-input' />",
                footerActionContent: "<button id='footer-button'>Footer button</button>"
            })).appendTo("#qunit-fixture");
        },
        clickBlanket: function() {
            // We don't want to include blanket.js - which creates the blanket - in our dependencies,
            // so create a mock blanket element
            var $blanket = AJS.$("<div></div>").addClass("aui-blanket").appendTo("#qunit-fixture");
            $blanket.click();
        }
    });

    test("Dialog2 focuses inside panel not on buttons", function() {
        var $el = AJS.$(aui.dialog.dialog2({
            headerActionContent: "<button id='my-button'>Button</button>",
            content: "<input type='text' id='my-input' />"
        })).appendTo("#qunit-fixture");
        var buttonFocusSpy = sinon.spy();
        AJS.$("#my-button").focus(buttonFocusSpy);
        var inputFocusSpy = sinon.spy();
        AJS.$("#my-input").focus(inputFocusSpy);

        var dialog = AJS.dialog2($el);
        dialog.show();

        equal(buttonFocusSpy.callCount, 0, "Expected my-button to never be focused");
        ok(inputFocusSpy.calledOnce, "Expected my-input to be focused");
    });

    test('Dialog focuses header if there are no other elements', function() {
        var $el = AJS.$(aui.dialog.dialog2({
            headerActionContent: "<button id='my-button'>Button</button>",
            content: "Some content (nothing you can focus)"
        })).appendTo("#qunit-fixture");
        var buttonFocusSpy = sinon.spy();
        AJS.$("#my-button").focus(buttonFocusSpy);

        var dialog = AJS.dialog2($el);
        dialog.show();

        equal(buttonFocusSpy.callCount, 1, "Expected my-button to be focused");
    });

    test("Dialog2 creates a blanket", function() {
        var $el = this.createInputContentEl();

        var dialog = AJS.dialog2($el);
        dialog.show();

        ok(AJS.dim.calledOnce, "Expected blanket to be shown");
    });

    test("Dialog2 remove on hide false", function() {
        var $el = this.createContentEl();
        var dialog = AJS.dialog2($el);
        dialog.show();

        dialog.hide();

        ok($el.parent().length);
    });

    test("Dialog2 remove on hide true", function() {
        var $el = this.createContentEl();
        $el.attr("data-aui-remove-on-hide","true");
        var dialog = AJS.dialog2($el);
        dialog.show();

        dialog.hide();

        ok(!$el.parent().length);
    });

    test("Dialog2 instance events on", function() {
        var $el = AJS.$(aui.dialog.dialog2({
            content: "Hello world"
        })).appendTo("#qunit-fixture");
        var dialog = AJS.dialog2($el);
        var spy = sinon.spy();
        dialog.on("hide", spy);

        dialog.show();
        dialog.hide();

        ok(spy.calledOnce, "Expected event listener called");
    });

    test("Dialog2 instance events can be turned off", function() {
        var $el = AJS.$(aui.dialog.dialog2({
            content: "Hello world"
        })).appendTo("#qunit-fixture");
        var dialog = AJS.dialog2($el);
        var spy = sinon.spy();
        dialog.on("hide", spy);
        dialog.off("hide", spy);

        dialog.show();
        dialog.hide();

        ok(spy.notCalled, "Expected event listener called");
    });

    test("Dialog2 global show event", function() {
        var $el = this.createContentEl();
        var dialog = AJS.dialog2($el);
        var spy = sinon.spy();

        AJS.dialog2.on("show", spy);
        dialog.show();
        AJS.dialog2.off("show", spy);

        ok(spy.calledOnce, "Expected event listener called");
        var $passedEl = spy.args[0][1];
        equal($passedEl[0], $el[0], "Expected first arg was dialog element");
    });

    test("Dialog2 global hide event", function() {
        var $el = this.createContentEl();
        var dialog = AJS.dialog2($el);
        var spy = sinon.spy();

        AJS.dialog2.on("hide", spy);
        dialog.show();
        dialog.hide();
        AJS.dialog2.off("hide", spy);

        ok(spy.calledOnce, "Expected event listener called");
        var $passedEl = spy.args[0][1];
        equal($passedEl[0], $el[0], "Expected first arg was dialog element");
    });

    test("Dialog2 global events can be turned off", function() {
        var $el = AJS.$(aui.dialog.dialog2({
            content: "Hello world"
        })).appendTo("#qunit-fixture");
        var dialog = AJS.dialog2($el);
        var spy = sinon.spy();

        AJS.dialog2.on("hide", spy);
        AJS.dialog2.off("hide", spy);
        dialog.show();
        dialog.hide();

        ok(spy.notCalled, "Expected event listener called");
    });

    test("Dialog2 two global events turned off correctly, same event name", function() {
        var $el = AJS.$(aui.dialog.dialog2({
            content: "Hello world"
        })).appendTo("#qunit-fixture");
        var dialog = AJS.dialog2($el);
        var spy = sinon.spy();
        var spy2 = sinon.spy();

        AJS.dialog2.on("hide", spy);
        AJS.dialog2.on("hide", spy2);
        AJS.dialog2.off("hide", spy);
        dialog.show();
        dialog.hide();
        AJS.dialog2.off("hide", spy2);

        ok(spy.notCalled, "Expected event listener called spy1");
        ok(spy2.calledOnce, "Expected event listener called spy2");
    });

    test("Dialog2 two global events turned off correctly, different event names", function() {
        var $el = AJS.$(aui.dialog.dialog2({
            content: "Hello world"
        })).appendTo("#qunit-fixture");
        var dialog = AJS.dialog2($el);
        var spy = sinon.spy();
        var spy2 = sinon.spy();

        AJS.dialog2.on("hide", spy);
        AJS.dialog2.on("show", spy2);
        AJS.dialog2.off("hide", spy);
        AJS.dialog2.off("show", spy2);
        dialog.show();
        dialog.hide();

        ok(spy.notCalled, "Expected event listener called spy1");
        ok(spy2.notCalled, "Expected event listener called spy2");
    });

    test("Dialog2 global beforeHide event cancels hide", function() {
        var $el = this.createContentEl();

        var dialog = AJS.dialog2($el);
        var stub = sinon.stub().returns(false);

        AJS.dialog2.on("beforeHide", stub);
        dialog.show();
        dialog.hide();
        AJS.dialog2.off("beforeHide", stub);

        ok(stub.calledOnce, "Expected bound handler was not called");
        ok(AJS.layer($el).isVisible(), "Expected first element hidden");
    });

    test("Dialog2 global beforeHide event cancels show", function() {
        var $el = this.createContentEl();

        var dialog = AJS.dialog2($el);
        var stub = sinon.stub().returns(false);

        AJS.dialog2.on("beforeShow", stub);
        dialog.hide();
        dialog.show();
        AJS.dialog2.off("beforeShow", stub);

        ok(stub.calledOnce, "Expected bound handler was not called");
        ok(!AJS.layer($el).isVisible(), "Expected first element hidden");
    });

    test("Dialog2 does not remove on hide when removeOnHide is false", function() {
        var $el = this.createContentEl();

        var dialog = AJS.dialog2($el);
        dialog.show();
        dialog.hide();

        ok($el.parent().length, "Dialog2 not removed when hide is called");
    });

    test("Dialog2 removes on hide when removeOnHide is true", function() {
        var $el = this.createContentEl();
        $el.attr("data-aui-remove-on-hide","true");

        var dialog = AJS.dialog2($el);
        dialog.show();
        dialog.hide();

        ok(!$el.parent().length, "Dialog2 removed when hide is called");
    });

    test("Dialog2 data-aui-remove-on-hide=false doesn't remove on escape", function() {
        var $el = this.createContentEl();

        var dialog = AJS.dialog2($el);
        dialog.show();

        this.pressEsc();

        ok($el.parent().length, "Dialog2 not removed when escape is pressed");
    });

    test("Dialog2 data-aui-remove-on-hide=true removes on escape", function() {
        var $el = this.createContentEl();
        $el.attr("data-aui-remove-on-hide","true");

        var dialog = AJS.dialog2($el);
        dialog.show();

        this.pressEsc();

        ok(!$el.parent().length, "Dialog2 removed when escape is pressed");
    });

    test("Dialog2 data-aui-remove-on-hide=false doesn't remove on blanket click", function() {
        var $el = this.createContentEl();

        var dialog = AJS.dialog2($el);
        dialog.show();

        this.clickBlanket();

        ok($el.parent().length, "Dialog2 not removed when blanked clicked");
    });

    test("Dialog2 data-aui-remove-on-hide=true removes on blanket click", function() {
        var $el = this.createContentEl();
        $el.attr("data-aui-remove-on-hide","true");

        var dialog = AJS.dialog2($el);
        dialog.show();

        this.clickBlanket();
        ok(!$el.parent().length, "Dialog2 removed when blanket clicked");
    });
});
