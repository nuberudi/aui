define(['tabs', 'soy/tabs', 'aui-qunit'], function() {
    module("Tabs Unit Tests", {
        DATA_TABS_PERSIST: "data-aui-persist",
        STORAGE_PREFIX: "_internal-aui-tabs-",
        persistAttribute: function(c) {
            return ((c.persist || c.persist === "")  ? " " + this.DATA_TABS_PERSIST + "=" + (c.persist ? c.persist : "\"\"") : "");
        },
        renderTemplate: function(c) {
            // TODO: render as soy ... (bitbucket.org/atlassian/soy-to-html-plugin ?)
            $("#qunit-fixture").append([
                "<" + (c.tagName || "div") + (c.id ? " id=" + c.id : "") + this.persistAttribute(c)  + " class='aui-tabs horizontal-tabs'>",
                "  <ul class='tabs-menu'>",
                "    <li class='menu-item active-tab'>",
                "      <a href='#tab-lister" + (c.tabPostfix || "") + "' id='menu-item-lister" + (c.tabPostfix || "") + "'><strong>Lister</strong></a>",
                "    </li>",
                "    <li class='menu-item'>",
                "      <a href='#tab-rimmer" + (c.tabPostfix || "") + "' id='menu-item-rimmer" + (c.tabPostfix || "") + "'><strong>Rimmer</strong></a>",
                "    </li>",
                "  </ul>",
                "  <" + (c.paneTagName || "div") + " id='tab-lister" + (c.tabPostfix || "") + "' class='tabs-pane active-pane'>",
                "    +1",
                "  </" + (c.paneTagName || "div") + ">",
                "  <" + (c.paneTagName || "div") + " id='tab-rimmer" + (c.tabPostfix || "") + "' class='tabs-pane'>",
                "    -1",
                "  </" + (c.paneTagName || "div") + ">",
                "</" + (c.tagName || "div") + ">"
            ].join(""));
        },
        addTabMenu: function() {
            // TODO: use soy
            $(".tabs-menu").append([
                "    <li class='menu-item'>",
                "      <a href='#tab-kryten' id='menu-item-kryten'><strong>Kryten</strong></a>",
                "    </li>"
            ].join(""));
        },
        addTabPane: function(c) {
            // TODO: use soy
            $(".aui-tabs").append([
                "  <" + (c.paneTagName || "div") + " id='tab-kryten' class='tabs-pane'>",
                "    +∞",
                "  </" + (c.paneTagName || "div") + ">"
            ].join(""));
        },
        teardown: function() {
            window.localStorage.clear();
        },
        createPersistentKey: function($tabGroup) {
            var tabGroupId = $tabGroup.attr("id");
            var value = $tabGroup.attr(this.DATA_TABS_PERSIST);

            return this.STORAGE_PREFIX + (tabGroupId ? tabGroupId : "") + (value && value !== "true" ? "-" + value : "");
        },
        getPersistent: function(groupId) {
            // groupId must exist in the dom
            var key = this.createPersistentKey($("#" + groupId));
            return window.localStorage.getItem(key);
        },
        storePersistent: function(groupId, value) {
            // groupId must exist in the dom
            var key = this.createPersistentKey($("#" + groupId));
            window.localStorage.setItem(key, value);
        }
    });

    test("Tabs API Test", function() {
           ok(typeof AJS.tabs == "object", "AJS.tabs exists!");
           ok(typeof AJS.tabs.setup == "function", "AJS.tabs.setup function exists!");
           ok(typeof AJS.tabs.change=="function", "AJS.tabs.change function exists!");
    });

    test("Clicking tab label hides active tab and shows new tab", function() {
        this.renderTemplate({});
        AJS.tabs.setup();

        $("#menu-item-rimmer").trigger("click");

        ok(!$("#tab-lister").is(":visible"), "Expected first tab should be hidden");
        ok($("#tab-rimmer").is(":visible"), "Expected second tab should be visible");
    });

    test("Tabs work when constructed with non-div tags", function() {
        this.renderTemplate({
            tagName: "section",
            paneTagName: "section"
        });
        AJS.tabs.setup();

        $("#menu-item-rimmer").trigger("click");

        ok(!$("#tab-lister").is(":visible"), "Expected first tab should be hidden");
        ok($("#tab-rimmer").is(":visible"), "Expected second tab should be visible");
    });

    test("Calling AJS.tabs.setup() multiple times does not double-bind event handlers", function() {
        this.renderTemplate({});
        AJS.tabs.setup();
        AJS.tabs.setup();
        var counter = 0;
        $(".tabs-menu a").bind("tabSelect", function() {
            ++counter;
        });
        $("#menu-item-rimmer").trigger("click");
        equal(counter, 1, "Expected change handler invoked only once");
    });

    test("Tabs should work if you add a tab after calling AJS.tabs.setup", function() {
        this.renderTemplate({});
        AJS.tabs.setup();
        this.addTabMenu();
        this.addTabPane({});

        $("#menu-item-kryten").trigger("click");

        ok(!$("#tab-lister").is(":visible"), "Expected first tab should be hidden");
        ok(!$("#tab-rimmer").is(":visible"), "Expected second tab should be hidden");
        ok($("#tab-kryten").is(":visible"), "Expected third tab should be visible");
    });

    test("Tabs soy template exists", function() {
        ok(aui.tabs);
    });

    test("Tabs soy template basic rendering", function() {
        var html = aui.tabs({
            menuItems : [{
            isActive : true,
            url : '#tab1',
            text : 'Tab 1'
        }, {
            url : '#tab2',
            text : 'Tab 2'
        }],
        paneContent : aui.tabPane({
            isActive : true,
            content : 'Tab 1 Content'
            }) + aui.tabPane({
                content : 'Tab 2 Content'
            })
        });

        var $el = $(html).appendTo(AJS.$("#qunit-fixture"));
        ok($el.is(".aui-tabs"));
    });

    test("Tabs no id persist=true doesnt save to local storage", function() {
        this.renderTemplate({persist:"true"});
        AJS.tabs.setup();

        $("#menu-item-rimmer").trigger("click");

        ok($("#tab-rimmer").is(":visible"), "Expected second tab should be visible");
        ok(!this.getPersistent("tabgroup1"), "Local storage shouldn't contain persist entry");
    });

    test("Tabs persist with no value saves to local storage", function() {
        this.renderTemplate({id:"tabgroup1", persist:""});
        AJS.tabs.setup();

        $("#menu-item-rimmer").trigger("click");

        ok($("#tab-rimmer").is(":visible"), "Expected second tab should be visible");
        equal(this.getPersistent("tabgroup1"), "menu-item-rimmer", "Local storage should contain persist entry");
    });

    test("Tabs persist=true saves to local storage", function() {
        this.renderTemplate({id:"tabgroup1", persist:"true"});
        AJS.tabs.setup();

        $("#menu-item-lister").trigger("click");
        $("#menu-item-rimmer").trigger("click");

        ok(!$("#tab-lister").is(":visible"), "Expected first tab should be hidden");
        ok($("#tab-rimmer").is(":visible"), "Expected second tab should be visible");

        equal(this.getPersistent("tabgroup1"), "menu-item-rimmer", "Local storage should contain persist entry");
    });

    test("Tabs persist with unique value saves to local storage", function() {
        this.renderTemplate({id:"tabgroup1", persist:"user1"});
        AJS.tabs.setup();

        $("#menu-item-rimmer").trigger("click");

        ok($("#tab-rimmer").is(":visible"), "Expected second tab should be visible");
        equal(this.getPersistent("tabgroup1"), "menu-item-rimmer", "Local storage should contain persist entry");
    });

    test("Tabs persist=false doesnt save to local storage", function() {
        this.renderTemplate({id:"tabgroup1", persist:"false"});
        AJS.tabs.setup();

        $("#menu-item-lister").trigger("click");
        $("#menu-item-rimmer").trigger("click");

        ok(!$("#tab-lister").is(":visible"), "Expected first tab should be hidden");
        ok($("#tab-rimmer").is(":visible"), "Expected second tab should be visible");

        ok(!this.getPersistent("tabgroup1"), "Local storage shouldn't contain persist entry");
    });

    test("Tabs no persist doesnt save to local storage", function() {
        this.renderTemplate({id:"tabgroup1"});
        AJS.tabs.setup();

        $("#menu-item-rimmer").trigger("click");

        ok($("#tab-rimmer").is(":visible"), "Expected second tab should be visible");
        ok(!this.getPersistent("tabgroup1"), "Local storage shouldn't contain persist entry");
    });

    test("Tabs persist=true saves to local storage multiple tab groups", function() {
        this.renderTemplate({id:"tabgroup1", persist:"true"});
        this.renderTemplate({id:"tabgroup2", persist:"true", tabPostfix:"2"});
        AJS.tabs.setup();

        $("#tabgroup1").find("#menu-item-lister").trigger("click");
        $("#tabgroup1").find("#menu-item-rimmer").trigger("click");
        $("#tabgroup2").find("#menu-item-rimmer2").trigger("click");
        $("#tabgroup2").find("#menu-item-lister2").trigger("click");

        equal(this.getPersistent("tabgroup1"), "menu-item-rimmer", "Local storage should contain group 1 persist entry");
        equal(this.getPersistent("tabgroup2"), "menu-item-lister2", "Local storage should contain group 2 persist entry");
    });

    test("Tabs persist=false doesnt load from local storage", function() {
        this.renderTemplate({id:"tabgroup1", persist:"false"});
        this.storePersistent("tabgroup1", "menu-item-rimmer");
        AJS.tabs.setup();

        ok($("#tab-lister").is(":visible"), "Expected first tab should be visible");
        ok(!$("#tab-rimmer").is(":visible"), "Expected second tab should be hidden");
    });

    test("Tabs persist=true loads from local storage", function() {
        this.renderTemplate({id:"tabgroup1", persist:"true"});
        this.storePersistent("tabgroup1", "menu-item-rimmer");

        AJS.tabs.setup();

        ok(!$("#tab-lister").is(":visible"), "Expected first tab should be hidden");
        ok($("#tab-rimmer").is(":visible"), "Expected second tab should be visible");
    });

    test("Tabs loading from local storage invokes tabSelect handler", function() {
        this.renderTemplate({id:"tabgroup1", persist:"true"});
        this.storePersistent("tabgroup1", "menu-item-rimmer");

        var counter = 0;
        $(".tabs-menu a").bind("tabSelect", function() {
            ++counter;
        });
        AJS.tabs.setup();

        equal(counter, 1, "Expected change handler invoked");
    });

    test("Tabs persist=true loads from local storage with multiple tab groups", function() {
        this.renderTemplate({id:"tabgroup1", persist:"true"});
        this.renderTemplate({id:"tabgroup2", persist:"true", tabPostfix:"2"});
        this.storePersistent("tabgroup1", "menu-item-rimmer");
        this.storePersistent("tabgroup2", "menu-item-rimmer2");

        AJS.tabs.setup();

        ok(!$("#tabgroup1").find("#tab-lister").is(":visible"), "Expected first group first tab should be hidden");
        ok($("#tabgroup1").find("#tab-rimmer").is(":visible"), "Expected first group second tab should be visible");
        ok(!$("#tabgroup2").find("#tab-lister2").is(":visible"), "Expected second group first tab should be hidden");
        ok($("#tabgroup2").find("#tab-rimmer2").is(":visible"), "Expected second group second tab should be visible");
    });
});