define(['event', 'aui-qunit'], function() {
    module("Events Unit Tests");

    test("Event Binding", function() {
        var spy = sinon.spy();
        AJS.bind('test1-event', spy);

        AJS.trigger('test1-event');

        ok(spy.calledOnce, "Expected bound handler called once");
    });

    test("Event Unbinding", function() {
        var spy = sinon.spy();
        AJS.bind('test2-event', spy);

        AJS.trigger('test2-event');
        ok(spy.calledOnce, "Expected bound handler called once");

        AJS.unbind('test2-event');
        AJS.trigger('test2-event');

        ok(spy.calledOnce, "Expected bound handler not called a second time");
    });
});