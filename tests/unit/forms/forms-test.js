
define(['forms', 'soy/form', 'aui-qunit'], function() {
    module("Forms Unit Tests", {
        setupInlineHelpTest: function() {
            AJS.$('#qunit-fixture').html(
                '<form class="aui">' +
                    '<span id="fieldHelpToggle" class="aui-icon icon-inline-help"><span>Help</span></span>' +
                    '<span id="field-help" class="field-help hidden">Inline help text here.</span>' +
                    '</form>'
            );
            AJS.inlineHelp();
        },
        setupDescriptionContentTest: function() {
            //Add HTML for escaped form description testing
            var descriptionContentFormHTML = aui.form.form({
                action : '#form',
                id : 'descriptionContent-text-field',
                content : aui.form.textField({
                    labelContent : 'Text Field, with description text (escaped)',
                    descriptionContent: 'This is some description <a href="example.com">with a link</a>, which should be escaped'
                })
            });
            AJS.$('#qunit-fixture').append(descriptionContentFormHTML);

        },
        setupDescriptionTextTest: function() {
            //Add HTML for escaped form description testing
            var descriptionTextFormHTML = aui.form.form({
                action : '#form',
                id : 'descriptionText-text-field',
                content : aui.form.textField({
                    labelContent : 'Text Field, with description text (escaped)',
                    descriptionText: 'This is some description <a href="example.com">with a link</a>, which should be escaped'
                })
            });
            AJS.$('#qunit-fixture').append(descriptionTextFormHTML);

        },

        setupShortDescriptionTest: function() {
            var descriptionContentFormHTML = aui.form.form({
                action : '#form',
                id : 'short-description-text-field',
                content : aui.form.textField({
                    labelContent : 'Text Field, with description text (short)',
                    descriptionContent: 'This is a short description'
                })
            });
            AJS.$('#qunit-fixture').append(descriptionContentFormHTML);
        }
    });

    test("Test inlineHelp", function() {
        this.setupInlineHelpTest();

        var fieldHelpToggle = AJS.$('#fieldHelpToggle');
        var fieldHelp = AJS.$('#field-help');

        ok(fieldHelp.hasClass('hidden'), "The fieldHelp element should have the hidden class");
        fieldHelpToggle.click();
        ok(!fieldHelp.hasClass('hidden'), "The fieldHelp element should not have the hidden class");
        fieldHelpToggle.click();
        ok(fieldHelp.hasClass('hidden'), "The fieldHelp element should have the hidden class again.");
    });



    test("Test descriptionContent", function () {
        this.setupDescriptionContentTest();

        var $descriptionContent = AJS.$("#qunit-fixture").find("#descriptionContent-text-field");
        var endAnchorUnescaped = "</a>";
        var endAnchorEscaped = "&lt;/a&gt;";


        //Content should be unescaped
        strictEqual($descriptionContent.find(".description").html().indexOf(endAnchorEscaped), -1
            , "The text field's descriptionContent has not been escaped");
        notStrictEqual($descriptionContent.find(".description").html().indexOf(endAnchorUnescaped), -1
            , "The text field's descriptionContent has been left unescaped");

    });

    test("Test descriptionText", function() {
        this.setupDescriptionTextTest();
        var $descriptionText = AJS.$("#qunit-fixture").find("#descriptionText-text-field");

        var endAnchorUnescaped = "</a>";
        var endAnchorEscaped = "&lt;/a&gt;";

        //Text should be escaped
        notStrictEqual($descriptionText.find(".description").html().indexOf(endAnchorEscaped), -1
            , "The text field's descriptionText has been escaped");
        strictEqual($descriptionText.find(".description").html().indexOf(endAnchorUnescaped), -1
            , "The text field's descriptionText has not been left unescaped");

    });

    test("Test short description", function() {
       this.setupShortDescriptionTest();
       var $shortDescription = AJS.$("#qunit-fixture").find("#short-description-text-field");

        //The field should contain the whole string (and nothing more)

        strictEqual($shortDescription.find(".description").html(),"This is a short description");
    });


    test("Test select template with default value", function() {
        AJS.$(aui.form.select({
            id: 'test-select',
            value: '2.5',
            options:
                [
                    {
                        text: '1.1',
                        value: '1.1'
                    },
                    {
                        text: '2.5',
                        value: '2.5'
                    }
                ]
        })).appendTo("#qunit-fixture");

        var value = AJS.$('#test-select').find(':selected').text();
        equal(value, '2.5', 'Expected option value to be selected');
    });

    test("Test select template with default value for optGroup", function() {
        AJS.$(aui.form.select({
            id: 'test-select',
            value: '3.5',
            options: [{
                text: 'group1',
                options:
                    [
                        {
                            text: '1.1',
                            value: '1.1'
                        },
                        {
                            text: '2.5',
                            value: '2.5'
                        }
                    ]
            },
                {
                    text: 'group2',
                    options:
                        [
                            {
                                text: '3.5',
                                value: '3.5'
                            },
                            {
                                text: '4.5',
                                value: '4.5'
                            }
                        ]
                }]
        })).appendTo("#qunit-fixture");

        var value = AJS.$('#test-select').find(':selected').text();
        equal(value, '3.5', 'Expected option value to be selected');
    });
});