define(['experimental-autocomplete/truncating-progressive-data-set', 'aui-qunit'], function() {
    var testSearch = function(item, term) {
        return item.get("username") && item.get("username").indexOf(term) > -1;
    };

    module('querying with remote sources that return truncated results', {
        setup: function() {
            var UserModel = Backbone.Model.extend({
               idAttribute: 'username'
            });
            this.url = '/dummy/search';
            this.data = new AJS.TruncatingProgressiveDataSet([], {
                model: UserModel,
                matcher: testSearch,
                maxResponseSize: 4,
                queryEndpoint: this.url,
                queryParamKey: 'username'
            });
            this.server = sinon.fakeServer.create();
        }, teardown: function() {
            this.server.restore();
        }
    });

    test('does re-request data when response is potentially truncated', function() {
        var callback = sinon.spy();
        this.data.bind('respond', callback);
        this.server.respondWith("GET", this.url + "?username=atl", [200, { "Content-Type": "application/json" }, '[{"username":"atl"},{"username":"atl2"},{"username":"atlas"},{"username":"atlas2"}]']);
        this.server.respondWith("GET", this.url + "?username=atlas", [200, { "Content-Type": "application/json" }, '[{"username":"atlas"},{"username":"atlas2"},{"username":"atlassian"}]']);

        this.data.query('atl');
        equal(this.server.requests.length, 1);
        equal(callback.callCount, 1);
        this.server.respond();
        equal(callback.callCount, 2);

        this.data.query('atlas');
        equal(callback.callCount, 3);
        equal(this.server.requests.length, 2, "we made an additional request");
        this.server.respond();

        equal(callback.callCount, 4);
        equal(callback.getCall(1).args[0].results[0].get("username"), "atl");
        equal(callback.getCall(1).args[0].results[1].get("username"), "atl2");
        equal(callback.getCall(1).args[0].results[2].get("username"), "atlas");
        equal(callback.getCall(3).args[0].results[0].get("username"), "atlas");
        equal(callback.getCall(3).args[0].results[1].get("username"), "atlas2");
        equal(callback.getCall(3).args[0].results[2].get("username"), "atlassian");
    });

    test('does not re-request data when response is not truncated', function() {
        var callback = sinon.spy();
        this.data.bind('respond', callback);
        this.server.respondWith("GET", this.url + "?username=atl", [200, { "Content-Type": "application/json" }, '[{"username":"atl"},{"username":"atl2"},{"username":"atlas"}]']);

        this.data.query('atl');
        equal(this.server.requests.length, 1);
        equal(callback.callCount, 1);
        this.server.respond();

        this.data.query('atlas');
        equal(callback.callCount, 3);

        equal(callback.getCall(1).args[0].results[0].get("username"), "atl");
        equal(callback.getCall(1).args[0].results[1].get("username"), "atl2");
        equal(callback.getCall(1).args[0].results[2].get("username"), "atlas");
        equal(callback.getCall(2).args[0].results[0].get("username"), "atlas");
    });

    test('ignores response size when query is completely different', function() {
        var callback = sinon.spy();
        this.data.bind('respond', callback);
        this.server.respondWith("GET", this.url + "?username=a", [200, { "Content-Type": "application/json" }, '[{"username":"atl"},{"username":"atl2"},{"username":"atlas"},{"username":"atlas2"}]']);
        this.server.respondWith("GET", this.url + "?username=b", [200, { "Content-Type": "application/json" }, '[{"username":"brian"}]']);

        this.data.query('a');
        equal(this.server.requests.length, 1);
        equal(callback.callCount, 1);
        this.server.respond();
        equal(callback.callCount, 2);

        this.data.query('b');
        equal(callback.callCount, 3);
        equal(this.server.requests.length, 2, "we made an additional request");
        this.server.respond();

        equal(callback.callCount, 4);
        equal(callback.getCall(1).args[0].results[0].get("username"), "atl");
        equal(callback.getCall(1).args[0].results[1].get("username"), "atl2");
        equal(callback.getCall(1).args[0].results[2].get("username"), "atlas");
        equal(callback.getCall(3).args[0].results[0].get("username"), "brian");
    });

    test('does not re-request if original request returned nothing', function() {
        var callback = sinon.spy();
        this.data.bind('respond', callback);
        this.server.respondWith("GET", this.url + "?username=a", [200, { "Content-Type": "application/json" }, '[]']);

        this.data.query('a');
        equal(this.server.requests.length, 1);
        equal(callback.callCount, 1);
        this.server.respond();
        equal(callback.callCount, 2);

        this.data.query('ab');
        equal(callback.callCount, 3);
        equal(this.server.requests.length, 1);

        equal(callback.getCall(1).args[0].results.length, 0);
        equal(callback.getCall(2).args[0].results.length, 0);
    });
});