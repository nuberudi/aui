define(['aui-select2', 'aui-qunit'], function() {
    module("AUI Select2 Unit Tests", {
        setup: function() {
            this.$fixture =  $("#qunit-fixture");
            $("<select />").appendTo(this.$fixture);
        }
    });

    test("test for presence of AUI Select2", function() {
        equal(typeof $.fn.auiSelect2, "function", "AUI Select2 exists");
    });

    test("test for creation of AUI Select2", function() {
        var $select = $("select", this.$fixture);
        $select.auiSelect2();
        ok($select.prev(".aui-select2-container").length > 0, "AUI Select2 was created");
    });

    // Extra spaces in class attribute will cause an <input type="hidden"> with select2 applied to it  to not close in FF
    // try the below example in the sandbox
    test("test for append custom AUI classes to select2 element", function() {
        var $input = $("<input>").attr("type", "hidden");
        $input.appendTo(this.$fixture);
        $input.auiSelect2({
            data: [
                {
                    id: 1,
                    text: 'one'
                },
                {
                    id: 2,
                    text: 'two'
                }
            ]
        });
        ok(!/\s\s+/.test($input.prev().attr('class')), "No extra spaces in AUI Select2 class attribute.");
    });
});