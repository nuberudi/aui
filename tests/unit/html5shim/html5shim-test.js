define(['html5-shim', 'aui-qunit'], function() {
    module("HTML5shim Unit Tests", {
        setup: function() {
            AJS.$('#qunit-fixture').html('<section id="html5">I am a section.<section>');
        }
    });

    test("html5 support / shim test in IE8 and below", function() {

        // check section tag has display block.
        var html5message,
            whatWeWant = "block",
            theBackground = AJS.$("#html5").css("display");

        if ( AJS.$.browser.msie && parseInt(AJS.$.browser.version) < 9 ) {
            html5message = " HTML5 elements should be supported by shim. ";
        } else {
            html5message = " HTML5 elements should be natively supported. ";
        }

        equal(theBackground, whatWeWant, html5message);

    });
});