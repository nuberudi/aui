define(['jquery', 'skate', 'trigger', 'aui-qunit'], function($, skate) {
    'use strict';

    var component;
    var element;
    var trigger;


    function click(num) {
        trigger.dispatchEvent(new CustomEvent('click'));
    }

    function createComponent() {
        return skate('#my-element', {
            show: function() {
                this.style.display = 'block';
                this.dispatchEvent(new CustomEvent('aui-after-show'));
            },

            hide: function() {
                this.style.display = 'none';
                this.dispatchEvent(new CustomEvent('aui-after-hide'));
            },
            isVisible: function() {
                return this.style.display === 'block';
            }
        });
    }

    function createElement() {
        return skate(
            $('<div id="my-element">some content</div>')
                .css({
                    display: 'none',
                    height: 100,
                    width: 100
                })
                .appendTo('#qunit-fixture')
                .get(0)
        );
    }

    function createTrigger(type) {
        return skate(
            $('<button data-aui-trigger="' + type + '" aria-controls="my-element"></button>')
                .appendTo('#qunit-fixture')
                .get(0)
        );
    }


    module('Trigger - Toggle', {
        setup: function() {
            component = createComponent();
            element = createElement();
            trigger = createTrigger('toggle');
        },

        teardown: function() {
            component.destroy();
        }
    });

    test('should toggle when clicked', function() {
        ok(!element.isVisible());
        click();
        ok(element.isVisible());
        click();
        ok(!element.isVisible());
    });
});
