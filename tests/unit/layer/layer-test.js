define(['layer', 'aui-qunit'], function() {
    module("Layer tests", {
        setup: function() {
            this.dimSpy = sinon.spy(AJS, "dim");
            this.undimSpy = sinon.spy(AJS, "undim");
            AJS.LayerManager.global = new AJS.LayerManager();
            this.focusManagerGlobalEnterSpy = sinon.spy(AJS.FocusManager.global, "enter");
            this.focusManagerGlobalExitSpy = sinon.spy(AJS.FocusManager.global, "exit");
        },
        teardown: function() {
            this.dimSpy.restore();
            this.undimSpy.restore();
            this.focusManagerGlobalEnterSpy.restore();
            this.focusManagerGlobalExitSpy.restore();
        },
        createLayer: function(blanketed) {
            var $el = AJS.$("<div></div>").addClass("aui-layer").attr("aria-hidden", "true").appendTo("#qunit-fixture");
            if (blanketed) {
                $el.attr("data-aui-blanketed", true);
            }
            //layer needs some height so that layer :visible selector works properly
            $el.height(100);
            return $el;
        }
    });

    test("Layer creates a div by default", function() {
        var layer = AJS.layer();

        ok(layer.$el.is("div.aui-layer"), "Expected a default element to be created");
        equal(layer.$el.attr("aria-hidden"), "true", "Expected a default layer to be created");
    });

    test("Layer works with passed element", function() {
        var $el = this.createLayer();
        var layer = AJS.layer($el);

        equal(layer.$el[0], $el[0], "Expected that the given element was bound");
    });

    test("Layer works with passed selector", function() {
      var $el = this.createLayer().attr("id", "my-layer");
      var layer = AJS.layer("#my-layer");

      equal(layer.$el[0], $el[0], "Expected that the given element was bound");
    });

    test("Layer works with DOM node", function() {
      var $el = this.createLayer();
      var layer = AJS.layer($el[0]);

      equal(layer.$el[0], $el[0], "Expected that the given element was bound");
    });

    test("Layer shows element", function() {
        var $el = this.createLayer();

        AJS.layer($el).show();

        equal($el.attr("aria-hidden"), "false", "Expected layer is not hidden");
    });

    test("Layer shows appends to body", function() {
        var $el = this.createLayer();

        AJS.layer($el).show();

        ok($el.parent().is("body"), "Layer is appended to body");
    });

    test("Layer shows sets z index", function() {
        var $el = this.createLayer();

        AJS.layer($el).show();

        equal($el.css("z-index"), 3000, "Layer z-index is initialised correctly");
    });

    test("Layer shows requests focus", function() {
        var $el = this.createLayer();

        AJS.layer($el).show();

        equal(AJS.FocusManager.global.enter.callCount, 1, "Focus is requested");
        var $focusEl = AJS.FocusManager.global.enter.args[0][0];
        equal($focusEl[0], $el[0], "Focus element is passed");
    });

    test("Layer hide hides element", function() {
        var $el = this.createLayer();
        var layer = AJS.layer($el);
        layer.show();

        layer.hide();

        equal($el.attr("aria-hidden"), "true", "Expected layer is hidden");
    });

    test("Layer hide forces blur", function() {
        var $el = this.createLayer();
        var layer = AJS.layer($el);
        layer.show();

        layer.hide();

        equal(AJS.FocusManager.global.exit.callCount, 1, "Blur is requested");
        var $focusEl = AJS.FocusManager.global.exit.args[0][0];
        equal($focusEl[0], $el[0], "Focus element is passed");
    });

    test("Layer hide blurs focus on multiple elements", function() {
        var $el1 = this.createLayer();
        var $el2 = this.createLayer();
        AJS.layer($el1).show();
        AJS.layer($el2).show();

        AJS.layer($el1).hide();

        equal(AJS.FocusManager.global.exit.callCount, 2, "Focus manager exit should have been called once");
        var $focusEl2 = AJS.FocusManager.global.exit.args[0][0];
        equal($focusEl2[0], $el2[0], "Focus manager exit was called with correct element");
        var $focusEl1 = AJS.FocusManager.global.exit.args[1][0];
        equal($focusEl1[0], $el1[0], "Focus manager exit was called with correct element");
    });

    test("Layer hide restores original z-index", function() {
        var $el = this.createLayer();
        $el.css("z-index", 1234);
        var layer = AJS.layer($el);
        layer.show();

        layer.hide();

        equal($el.css("z-index"), 1234, "Original z-index is restored");
    });

    test("Layer remove removes element", function() {
        var $el = this.createLayer();
        var layer = AJS.layer($el);
        layer.show();

        layer.remove();

        equal($el.attr("aria-hidden"), "true", "Expected layer is hidden");
        ok(!$el.parent().length, "Layer el should have no parent");
    });

    test("Layer changeSize to content height", function() {
        var $el = this.createLayer($el);
        AJS.$("<div></div>").css("height", 99999).appendTo($el);
        var layer = AJS.layer($el);
        layer.show();

        layer.changeSize(666, "content");

        equal($el.width(), 666, "Width should be set");
        // test based on the height of the inner contents
        ok($el.attr("style").indexOf("height") < 0, "Height should be empty");
    });

    test("Layer changeSize to fixed height", function() {
        var $el = this.createLayer();
        var layer = AJS.layer($el);
        layer.show();

        layer.changeSize(666, 999);

        equal($el.width(), 666, "Width should be set");
        equal($el.height(), 999, "Height should be set");
    });

    test("Layer hide hides multiple elements", function() {
        var $el1 = this.createLayer();
        var $el2 = this.createLayer();
        AJS.layer($el1).show();
        AJS.layer($el2).show();

        AJS.layer($el1).hide();

        equal($el1.attr("aria-hidden"), "true", "Expected layer is hidden");
        equal($el2.attr("aria-hidden"), "true", "Expected layer is hidden");
    });

    test("Layer increases z-index for multiple layers", function() {
        var $el1 = this.createLayer();
        var $el2 = this.createLayer();
        AJS.layer($el1).show();

        AJS.layer($el2).show();

        equal($el2.css("z-index"), 3100, "z-index should be set higher than first layer");
    });

    test("Layer does not create blanket by default", function() {
        var $el = this.createLayer();
        AJS.layer($el).show();

        ok(!this.dimSpy.called, "Dim should not be called");
    });

    test("Layer creates blanket with specific z-index when specified", function() {
        var $el = this.createLayer(true);
        AJS.layer($el).show();

        ok(this.dimSpy.calledOnce, "Dim should have been called");
        equal(this.dimSpy.args[0][1], 2980, "Dim was called with specific z-index");
    });

    test("Layer does not create blanket when data-aui-blanketed='false'", function() {
        var $el = this.createLayer().attr("data-aui-blanketed", "false");
        AJS.layer($el).show();

        ok(!this.dimSpy.called, "Dim should not be called");
    });

    test("Layer creates blanket with specific z-index for multiple layers", function() {
        var $el1 = this.createLayer(true);
        var $el2 = this.createLayer(true);
        AJS.layer($el1).show();
        AJS.layer($el2).show();

        equal(this.dimSpy.args[1][1], 3080, "Dim was called with specific z-index");
    });

    test("Layer pops dim and re-adds for stacked layers when specified", function() {
        var $el1 = this.createLayer(true);
        var $el2 = this.createLayer(true);
        AJS.layer($el1).show();
        AJS.layer($el2).show();

        equal(this.dimSpy.callCount, 2, "Dim should have been called twice");
        equal(this.undimSpy.callCount, 1, "Undim should have been called once");
    });

    test("Layer pops dim and re-adds for stacked layers when specified when some have no blanket", function() {
        var $el1 = this.createLayer(true);
        var $el2 = this.createLayer();
        var $el3 = this.createLayer();
        var $el4 = this.createLayer(true);
        AJS.layer($el1).show();
        AJS.layer($el2).show();
        AJS.layer($el3).show();
        AJS.layer($el4).show();

        equal(this.dimSpy.callCount, 2, "Dim should have been called twice");
        equal(this.undimSpy.callCount, 1, "Undim should have been called once");
    });

    test("Layer pops down to a given element when many are layered", function() {
        var $el1 = this.createLayer(true);
        var $el2 = this.createLayer();
        AJS.layer($el1).show();
        AJS.layer($el2).show();

        AJS.layer($el1).hide();

        equal($el1.attr("aria-hidden"), "true", "All layers should have been hidden");
        equal($el2.attr("aria-hidden"), "true", "All layers should have been hidden");
    });

    test("Layer pop multilayer", function() {
        var $el1 = this.createLayer(true);
        var $el2 = this.createLayer();
        var $el3 = this.createLayer(true);

        AJS.layer($el1).show();
        AJS.layer($el2).show();
        AJS.layer($el3).show();
        AJS.layer($el3).hide();

        equal(this.dimSpy.callCount, 3, "Dim should have been called thrice");
        equal(this.dimSpy.args[0][1], 2980, "Dim should have been called with specific z-index");
        equal(this.dimSpy.args[1][1], 3180, "Dim should have been called with specific z-index");
        equal(this.dimSpy.args[2][1], 2980, "Dim should have been called with specific z-index");
    });

    test("Layer popTop with empty", function() {
        ok(!AJS.LayerManager.global.popTopIfNonModal(), "Expected null response");
    });

    test("Layer popTop one element", function() {
        var $el = this.createLayer();
        AJS.layer($el).show();

        var $topLayer = AJS.LayerManager.global.popTopIfNonModal();

        ok($topLayer.attr("aria-hidden"), "true", "Expected first element hidden");
    });

    test("Layer popTop two elements", function() {
        var $el1 = this.createLayer();
        var $el2 = this.createLayer();
        AJS.layer($el1).show();
        AJS.layer($el2).show();

        var $topLayer = AJS.LayerManager.global.popTopIfNonModal();

        equal($topLayer[0], $el2[0], "Expected second element to be returned");
        ok($el1.attr("aria-hidden"), "false", "Expected first element visible");
        ok($el2.attr("aria-hidden"), "true", "Expected second element hidden");
    });

    test("Layer popTop modal", function() {
        var $el = this.createLayer().attr("data-aui-modal", true);
        AJS.layer($el).show();

        var $topLayer = AJS.LayerManager.global.popTopIfNonModal();

        ok(!$topLayer, "Expected no element to be returned");
        ok($el.attr("aria-hidden"), "false", "Expected first element visible");
    });

    test("Layer popUntilTopBlanketed with empty", function() {
        ok(!AJS.LayerManager.global.popUntilTopBlanketed(), "Expected null response");
    });

    test("Layer popUntilTopBlanketed one element", function() {
        var $el = this.createLayer(true);
        AJS.layer($el).show();

        var $topLayer = AJS.LayerManager.global.popUntilTopBlanketed();

        equal($topLayer[0], $el[0], "Expected single element to be returned");
        ok($el.attr("aria-hidden"), "true", "Expected first element hidden");
    });

    test("Layer popUntilTopBlanketed with no blanketed element", function() {
        var $el = this.createLayer();
        AJS.layer($el).show();

        var $topLayer = AJS.LayerManager.global.popUntilTopBlanketed();

        ok(!$topLayer, "Expected no element to be returned");
        ok($el.attr("aria-hidden"), "false", "Expected first element visible");
    });

    test("Layer popUntilTopBlanketed two elements", function() {
        var $el1 = this.createLayer(true);
        var $el2 = this.createLayer(true);
        AJS.layer($el1).show();
        AJS.layer($el2).show();

        var $topLayer = AJS.LayerManager.global.popUntilTopBlanketed();

        equal($topLayer[0], $el2[0], "Expected second element to be returned");
        ok($el1.attr("aria-hidden"), "false", "Expected first element visible");
        ok($el2.attr("aria-hidden"), "true", "Expected second element hidden");
    });

    test("Layer popUntilTopBlanketed with layered non-blanketed elements", function() {
        var $el1 = this.createLayer(true);
        var $el2 = this.createLayer(true);
        var $el3 = this.createLayer();
        AJS.layer($el1).show();
        AJS.layer($el2).show();
        AJS.layer($el3).show();

        var $topLayer = AJS.LayerManager.global.popUntilTopBlanketed();

        equal($topLayer[0], $el2[0], "Expected top blanketed element to be returned");
        ok($el1.attr("aria-hidden"), "false", "Expected first element visible");
        ok($el2.attr("aria-hidden"), "true", "Expected second element hidden");
        ok($el3.attr("aria-hidden"), "true", "Expected third element hidden");
    });

    test("Layer popUntilTopBlanketed modal", function() {
        var $el = this.createLayer(true).attr("data-aui-modal", true);
        AJS.layer($el).show();

        var $topLayer = AJS.LayerManager.global.popUntilTopBlanketed();

        ok(!$topLayer, "Expected no element to be returned");
        ok($el.attr("aria-hidden"), "false", "Expected first element visible");
    });

    test("Layer LayerManager barfs if the same element is layered twice", function() {
        var $el = this.createLayer();
        AJS.layer($el).show();

        throws(function() {
            AJS.LayerManager.global.push($el);
        }, "Should throw an exception");
    });

    test("Layer show triggers show event", function() {
        var layer = AJS.layer(this.createLayer());
        var spy = sinon.spy();
        layer.on("show", spy);

        layer.show();

        ok(spy.calledOnce, "Bound show event handler was called");
    });

    test("Layer hide triggers hide event", function() {
        var layer = AJS.layer(this.createLayer());
        var spy = sinon.spy();
        layer.on("hide", spy);
        layer.show();

        layer.hide();

        ok(spy.calledOnce, "Bound hide event handler was called");
    });

    test("Layer instance events can be turned off", function() {
        var layer = AJS.layer(this.createLayer());
        var spy = sinon.spy();
        layer.on("hide", spy);
        layer.off("hide", spy);

        layer.show();
        layer.hide();

        ok(spy.notCalled, "Expected event listener called");
    });

    test("Layer hide can trigger multiple hide events", function() {
        var layer1 = AJS.layer(this.createLayer());
        var spy1 = sinon.spy();
        layer1.on("hide", spy1).show();
        var layer2 = AJS.layer(this.createLayer());
        var spy2 = sinon.spy();
        layer2.on("hide", spy2).show();

        layer1.hide();

        ok(spy1.calledOnce, "Bound hide event handler was called");
        ok(spy2.calledOnce, "Bound hide event handler was called");
    });

    test("Layer show triggers global event", function() {
        var $el = this.createLayer();
        var layer = AJS.layer($el);
        var spy = sinon.spy();
        AJS.layer.on("show", spy);

        layer.show();

        ok(spy.calledOnce, "Global show event handler was called");
        var $boundEl = spy.args[0][1];
        equal($boundEl[0], $el[0], "Global show event handler was called with single arg");
    });

    test("Layer hide triggers global event", function() {
        var $el = this.createLayer();
        var layer = AJS.layer($el);
        var spy = sinon.spy();
        AJS.layer.on("hide", spy);
        layer.show();

        layer.hide();

        ok(spy.calledOnce, "Global hide event handler was called");
        var $boundEl = spy.args[0][1];
        equal($boundEl[0], $el[0], "Global hide event handler was called with single arg");
    });

    test("Layer global events can be turned off", function() {
        var layer = AJS.layer(this.createLayer());
        var spy = sinon.spy();

        AJS.layer.on("hide", spy);
        AJS.layer.off("hide", spy);
        layer.show();
        layer.hide();

        ok(spy.notCalled, "Expected event listener called");
    });

    test("Layer two global events turned off correctly, same event name", function() {
        var layer = AJS.layer(this.createLayer());
        var spy = sinon.spy();
        var spy2 = sinon.spy();

        AJS.layer.on("hide", spy);
        AJS.layer.on("hide", spy2);
        AJS.layer.off("hide", spy);
        layer.show();
        layer.hide();
        AJS.layer.off("hide", spy2);

        ok(spy.notCalled, "Expected event listener called spy1");
        ok(spy2.calledOnce, "Expected event listener called spy2");
    });

    test("Layer beforeShow event displays", function() {
        var $el = this.createLayer();
        var layer = AJS.layer($el);
        var stub = sinon.stub().returns(true);
        layer.on("beforeShow", stub);

        layer.show();

        ok(stub.calledOnce, "Expected bound handler was called");
        ok(layer.isVisible(), "Expected first element visible");
    });

    test("Layer beforeShow event cancels display", function() {
        var $el = this.createLayer();
        var layer = AJS.layer($el);
        var stub = sinon.stub().returns(false);
        layer.on("beforeShow", stub);

        layer.show();

        ok(stub.calledOnce, "Expected bound handler was called");
        ok(!layer.isVisible(), "Expected first element hidden");
    });

    test("Layer beforeShow event cancels display with multiple handlers", function() {
        var $el = this.createLayer();
        var layer = AJS.layer($el);
        var stub = sinon.stub().returns(false);
        var stub2 = sinon.stub().returns(true);
        layer.on("beforeShow", stub);
        layer.on("beforeShow", stub2);

        layer.show();

        ok(stub.calledOnce, "Expected bound handler was called");
        ok(stub2.calledOnce, "Expected bound handler was called");
        ok(!layer.isVisible(), "Expected first element hidden");
    });

    test("Layer beforeHide event hides", function() {
        var $el = this.createLayer();
        var layer = AJS.layer($el);
        var stub = sinon.stub().returns(true);
        layer.on("beforeHide", stub);

        layer.show();
        layer.hide();

        ok(stub.calledOnce, "Expected bound handler was called");
        ok(!layer.isVisible(), "Expected first element hidden");
    });

    test("Layer beforeHide event cancels hide", function() {
        var $el = this.createLayer();
        var layer = AJS.layer($el);
        var stub = sinon.stub().returns(false);
        layer.on("beforeHide", stub);

        layer.show();
        layer.hide();

        ok(stub.calledOnce, "Expected bound handler was called");
        ok(layer.isVisible(), "Expected first element hidden");
    });


    test("Layer beforeHide event cancels hide with multiple handlers", function() {
        var $el = this.createLayer();
        var layer = AJS.layer($el);
        var stub = sinon.stub().returns(true);
        var stub2 = sinon.stub().returns(false);
        layer.on("beforeHide", stub);
        layer.on("beforeHide", stub2);

        layer.show();
        layer.hide();

        ok(stub.calledOnce, "Expected bound handler was called");
        ok(stub2.calledOnce, "Expected bound handler was called");
        ok(layer.isVisible(), "Expected first element hidden");
    });

    test("Layer global beforeHide event cancels hide", function() {
        var $el = this.createLayer();
        var layer = AJS.layer($el);
        var stub = sinon.stub().returns(false);
        AJS.layer.on("beforeHide", stub);

        layer.show();
        layer.hide();

        AJS.layer.off("beforeHide", stub);

        ok(stub.calledOnce, "Expected bound handler was not called");
        ok(layer.isVisible(), "Expected first element hidden");
    });

    test("Layer global beforeHide event cancels hide with multiple handlers", function() {
        var $el = this.createLayer();
        var layer = AJS.layer($el);
        var stub = sinon.stub().returns(false);
        var stub2 = sinon.stub().returns(true);
        AJS.layer.on("beforeHide", stub);
        AJS.layer.on("beforeHide", stub2);

        layer.show();
        layer.hide();

        AJS.layer.off("beforeHide", stub);
        AJS.layer.off("beforeHide", stub2);

        ok(stub.calledOnce, "Expected bound handler was called");
        ok(stub2.calledOnce, "Expected bound handler was called");
        ok(layer.isVisible(), "Expected first element hidden");
    });

    test("Layer global beforeShow event cancels show", function() {
        var $el = this.createLayer();
        var layer = AJS.layer($el);
        var stub = sinon.stub().returns(false);
        AJS.layer.on("beforeShow", stub);

        layer.hide();
        layer.show();

        AJS.layer.off("beforeShow", stub);

        ok(stub.calledOnce, "Expected bound handler was not called");
        ok(!layer.isVisible(), "Expected first element hidden");
    });

    test("Layer global beforeShow event cancels show with multiple handlers", function() {
        var $el = this.createLayer();
        var layer = AJS.layer($el);
        var stub = sinon.stub().returns(false);
        var stub2 = sinon.stub().returns(true);
        AJS.layer.on("beforeShow", stub);
        AJS.layer.on("beforeShow", stub2);

        layer.hide();
        layer.show();

        AJS.layer.off("beforeShow", stub);
        AJS.layer.off("beforeShow", stub2);

        ok(stub.calledOnce, "Expected bound handler was called");
        ok(stub2.calledOnce, "Expected bound handler was called");
        ok(!layer.isVisible(), "Expected first element hidden");
    });

    test("Layer isVisible returns true", function() {
        var $el = this.createLayer();
        var layer = AJS.layer($el);

        layer.hide();

        ok(!layer.isVisible(), "Expected first element hidden");
    });

    test("Layer isVisible returns true", function() {
        var $el = this.createLayer();
        var layer = AJS.layer($el);

        layer.show();

        ok(layer.isVisible(), "Expected first element visible");
    });
});