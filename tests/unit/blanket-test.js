define(['blanket', 'aui-qunit'], function() {
    module('Blanket');

    test("test no overflow remains at default body", function() {
        AJS.$("body").css({"overflow": ""});
        AJS.dim();
        AJS.undim();
        equal(AJS.$("body").css("overflow"), "visible", "Overflow remained at its default");
    });

    test("test no overflow remains hidden body", function() {
        AJS.$("body").css({"overflow": "hidden"});
        AJS.dim();
        AJS.undim();
        equal(AJS.$("body").css("overflow"), "hidden", "Overflow remained at its default");
    });

    test("test no overflow remains at default html", function() {
        //have to query the dom for the default in this case because ie7 does not adhere to the spec
        var overrideDefault = AJS.$("html").css("overflow");
        AJS.dim();
        AJS.undim();
        equal(AJS.$("html").css("overflow"), overrideDefault, "Overflow is empty");
    });

    test("test no overflow remains hidden html", function() {
        AJS.$("html").css({"overflow": "hidden"});
        AJS.dim();
        AJS.undim();
        equal(AJS.$("html").css("overflow"), "hidden", "Overflow remained hidden");
    });
});