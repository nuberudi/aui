(function() {

    function addFixtureToBody() {
        AJS.$('<div id="qunit-fixture"></div>').appendTo(document.body);
    }

    function removeFixtureFromBody() {
        AJS.$('#qunit-fixture').remove();
    }

    function getLayers() {
        return AJS.$('.aui-layer');
    }

    function removeLayers() {
        getLayers().remove();
    }

    function warnIfLayersExist(test) {
        if (getLayers().length) {
            //need to bind to console for chrome, otherwise it throws an illegal invocation error
            console.log(
                test.module
                    + ': '
                    + test.name
                    + ' - '
                    + "Layers have been left in the DOM. These must be removed from the BODY to ensure they don't affect other tests. Use AJS.qunit.removeLayers()."
            );
            removeLayers();
        }
    }

    /**
     * Karma does not create a qunit-fixture element by default - use this to setup and teardown the fixture.
     */
    QUnit.testStart(function(test) {
        addFixtureToBody();
    });
    QUnit.testDone(function(test) {
        warnIfLayersExist(test);
        removeFixtureFromBody();
    });

    AJS.qunit = {
        removeLayers: removeLayers
    };

})();