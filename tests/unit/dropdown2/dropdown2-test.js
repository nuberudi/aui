define(['dropdown2', 'soy/dropdown2', 'aui-qunit'], function() {
    module("Dropdown2 Unit Tests", {
        setup: function () {
            AJS.$('#qunit-fixture').html(
                '<ul class="aui-dropdown2-trigger-group">' +
                    '<li>' +
                        '<a id="dropdown2-trigger" aria-owns="dropdown2-test" aria-haspopup="true" class="aui-dropdown2-trigger" href="#dropdown2-test" >Example dropdown</a>' +
                    '</li>' +
                    '<li>' +
                        '<a id="dropdown2-trigger2" aria-owns="dropdown2-test2" aria-haspopup="true" class="aui-dropdown2-trigger" href="#dropdown2-test2" >Example second dropdown</a>' +
                    '</li>' +
                '</ul>' +
                '<div id="dropdown2-test" class="aui-dropdown2"></div>' +
                '<div id="dropdown2-test2" class="aui-dropdown2"></div>' +
                '<div id="hideout"></div>'
            );
            this.$trigger = AJS.$("#dropdown2-trigger");
            this.$trigger2 = AJS.$("#dropdown2-trigger2");
            this.$content = AJS.$("#dropdown2-test");
            this.$content2 = AJS.$("#dropdown2-test2");
            this.$hideout = AJS.$("#hideout");

            this.clock = sinon.useFakeTimers();
        },
        teardown: function () {
            this.clock.restore();
        },
        simulateTriggerClick: function(num) {
            this['$trigger' + (num || '')].trigger('click');
        },
        simulateTriggerHover: function(num) {
            var e = jQuery.Event("mousemove");
            this['$trigger' + (num || '')].trigger(e);
            this.clock.tick(0);
        },
        invokeTrigger: function () {
            this.$trigger.trigger("aui-button-invoke");
            this.clock.tick(100);
        },
        simulateItemClick: function($item) {
            $item.trigger('click');
        },
        pressUp: function() {
            var e = jQuery.Event("keydown");
            e.keyCode = AJS.keyCode.UP;
            $(document).trigger(e);
        },
        pressDown: function() {
            var e = jQuery.Event("keydown");
            e.keyCode = AJS.keyCode.DOWN;
            $(document).trigger(e);
        },
        pressLeft: function() {
            var e = jQuery.Event("keydown");
            e.keyCode = AJS.keyCode.LEFT;
            $(document).trigger(e);
        },
        pressRight: function() {
            var e = jQuery.Event("keydown");
            e.keyCode = AJS.keyCode.RIGHT;
            $(document).trigger(e);
        },
        triggerIsActive: function(num) {
            return this['$trigger' + (num || '')].is('.active,.aui-dropdown2-active');
        },
        item: function(itemId, num) {
            return this['$content' + (num || '')].find('#' + itemId);
        },
        addPlainSection: function() {
            AJS.$("#section1").remove();
            AJS.$('#dropdown2-test').append(
                '<div id="section1" class="aui-dropdown2-section">' +
                    '<ul>' +
                        '<li><a id="item1">Menu item</a></li>' +
                        '<li><a id="item2">Menu item</a></li>' +
                        '<li><a id="item3">Menu item</a></li>' +
                        '<li><a id="item4">Menu item</a></li>' +
                    '</ul>' +
                '</div>'
            );
        },
        addPlainSection2: function() {
            AJS.$("#section12").remove();
            AJS.$('#dropdown2-test2').append(
                '<div id="section12" class="aui-dropdown2-section">' +
                    '<ul>' +
                        '<li><a id="item12">Menu item</a></li>' +
                        '<li><a id="item22">Menu item</a></li>' +
                        '<li><a id="item32">Menu item</a></li>' +
                        '<li><a id="item42">Menu item</a></li>' +
                    '</ul>' +
                '</div>'
            );
        },
        addRadioSection: function() {
            AJS.$("#section2").remove();
            AJS.$('#dropdown2-test').append(
                '<div id="section2" class="aui-dropdown2-section">' +
                    '<ul role="radiogroup">' +
                        '<li><a id="radio1-unchecked" class="aui-dropdown2-radio interactive aui-dropdown2-interactive">Menu item</a></li>' +
                        '<li><a id="radio2-checked" class="aui-dropdown2-radio interactive aui-dropdown2-interactive checked">Menu item</a></li>' +
                        '<li><a id="radio3-unchecked" class="aui-dropdown2-radio interactive aui-dropdown2-interactive">Menu item</a></li>' +
                    '</ul>' +
                '</div>'
            );
        },
        addHiddenSection: function() {
            AJS.$("#section3").remove();
            AJS.$('#dropdown2-test').append(
                '<div id="section3" class="aui-dropdown2-section">' +
                    '<ul>' +
                        '<li class="aui-dropdown2-hidden"><a id="hidden1-unchecked-disabled" class="aui-dropdown2-checkbox interactive disabled" >Menu item</a></li>' +
                        '<li class="aui-dropdown2-hidden"><a id="hidden2-checked" class="aui-dropdown2-checkbox interactive aui-dropdown2-checked">Menu item</a></li>' +
                    '</ul>' +
                '</div>'
            );
        },
        addInteractiveSection: function() {
            AJS.$("#section4").remove();
            AJS.$('#dropdown2-test').append(
                '<div id="section4" class="aui-dropdown2-section">' +
                    '<ul role="radiogroup">' +
                        '<li><a id="iradio1-interactive-checked" class="aui-dropdown2-radio interactive aui-dropdown2-interactive checked">Menu item</a></li>' +
                        '<li><a id="iradio2-interactive-unchecked" class="aui-dropdown2-radio interactive">Menu item</a></li>' +
                        '<li><a id="iradio3-unchecked" class="aui-dropdown2-radio">Menu item</a></li>' +
                    '</ul>' +
                '</div>'
            );
        },
        addCheckboxSection: function() {
            AJS.$("#section5").remove();
            AJS.$('#dropdown2-test').append(
                '<div id="section5" class="aui-dropdown2-section">' +
                    '<ul>' +
                        '<li><a id="check1-unchecked" class="aui-dropdown2-checkbox interactive">Menu item</a></li>' +
                        '<li><a id="check2-checked" class="aui-dropdown2-checkbox interactive checked">Menu item</a></li>' +
                        '<li><a id="check3-unchecked" class="aui-dropdown2-checkbox interactive">Menu item</a></li>' +
                    '</ul>' +
                '</div>'
            );
        }
    });

    // Testing opening and closing by triggering invoke

    test("Dropdown2 aui-button-invoke opens dropdown", function() {
        this.addPlainSection();
        this.invokeTrigger();

        ok(this.triggerIsActive(), "Dropdown2 is active");
    });

    test("Dropdown2 aui-button-invoke closes dropdown", function() {
        this.addPlainSection();

        this.invokeTrigger();
        this.invokeTrigger();
        
        ok(!this.triggerIsActive(), "Dropdown2 is not active 2");
    });

    test("Dropdown2 does not open when disabled", function() {
        this.addPlainSection();
        this.$trigger.attr('aria-disabled','true');

        this.simulateTriggerClick();

        ok(!this.triggerIsActive(), "Dropdown2 is active");
    });

    // Testing key navigation

    test("Dropdown2 navigated correctly using keys", function() {
        this.addPlainSection();
        this.simulateTriggerClick();

        this.pressDown();

        var $i1 = this.item('item1'),
            $i2 = this.item('item2');

        ok(this.triggerIsActive(), "Dropdown2 is active");
        ok(!$i1.is('.aui-dropdown2-active'), "Dropdown2 item 1 is not active");
        ok($i2.is('.aui-dropdown2-active'), "Dropdown2 item 2 is active");
    });

    test("Dropdown2 navigated correctly using keys with hidden items", function() {
        this.addPlainSection();

        var $i1 = this.item('item1'),
            $i2 = this.item('item2'),
            $i3 = this.item('item3'),
            $i4 = this.item('item4');

        $i1.parent().addClass('hidden');
        $i3.parent().addClass('hidden');

        this.simulateTriggerClick();
        this.pressDown();

        ok(!$i2.is('.aui-dropdown2-active'), "Dropdown2 item 2 not active");
        ok($i4.is('.aui-dropdown2-active'), "Dropdown2 item 4 is active");
    });

    test("Dropdown2 navigated correctly using keys with hidden items that were added after opened", function() {
        this.addPlainSection();

        var $i1 = this.item('item1'),
            $i2 = this.item('item2'),
            $i3 = this.item('item3'),
            $i4 = this.item('item4');

        $i1.parent().addClass('hidden');

        this.simulateTriggerClick();
        $i3.attr('aria-disabled','true').addClass('disabled').parent().addClass('hidden');
        this.pressDown();

        ok(!$i2.is('.aui-dropdown2-active'), "Dropdown2 item 2 was skipped");
        ok($i4.is('.aui-dropdown2-active'), "Dropdown2 item 4 was skipped");
    });

    // --- Testing Events ---

    // Show Events

    test("Dropdown2 fires aui-dropdown2-show when it is shown by click", function() {
        this.addPlainSection();

        var spy = sinon.spy();
        this.$content.on("aui-dropdown2-show", spy);

        this.simulateTriggerClick();

        ok(spy.calledOnce, "Expected event listener called on show");
    });

    test("Dropdown2 fires aui-dropdown2-show when it is shown by invoke", function() {
        this.addPlainSection();

        var spy = sinon.spy();
        this.$content.on("aui-dropdown2-show", spy);

        this.invokeTrigger();

        ok(spy.calledOnce, "Expected event listener called on show");
    });

    // Hide Events

    test("Dropdown2 fires aui-dropdown2-hide when it is hidden by click", function() {
        this.addPlainSection();
        var spy = sinon.spy();
        this.$content.on("aui-dropdown2-hide", spy);

        this.simulateTriggerClick();
        this.clock.tick(100);
        this.simulateTriggerClick();

        ok(spy.calledOnce, "Expected event listener called on hide");
    });

    test("Dropdown2 fires aui-dropdown2-hide when it is hidden by invoke", function() {
        this.addPlainSection();
        var spy = sinon.spy();
        this.$content.on("aui-dropdown2-hide", spy);

        this.simulateTriggerClick();
        this.invokeTrigger();

        ok(spy.calledOnce, "Expected event listener called on hide");
    });

    // Check and uncheck Events on checkboxes and radios

    test("Dropdown2 fires aui-dropdown2-item-check on checkboxes", function() {
        this.addCheckboxSection();
        this.simulateTriggerClick();
        var spy = sinon.spy();
        this.$content.on("aui-dropdown2-item-check", spy);

        var $c1 = this.item('check1-unchecked'),
            $c2 = this.item('check2-checked'),
            $c3 = this.item('check3-unchecked');

        this.simulateItemClick($c1);
        this.simulateItemClick($c2);
        this.simulateItemClick($c3);

        ok(spy.calledTwice, "Expected event listener called on hide");
    });

    test("Dropdown2 fires aui-dropdown2-item-uncheck on checkboxes", function() {
        this.addCheckboxSection();
        this.simulateTriggerClick();
        var spy = sinon.spy();
        this.$content.on("aui-dropdown2-item-uncheck", spy);

        var $c1 = this.item('check1-unchecked'),
            $c2 = this.item('check2-checked'),
            $c3 = this.item('check3-unchecked');

        this.simulateItemClick($c1);
        this.simulateItemClick($c2);
        this.simulateItemClick($c3);

        ok(spy.calledOnce, "Expected event listener called on hide");
    });

    test("Dropdown2 fires aui-dropdown2-item-check on radios", function() {
        this.addRadioSection();
        this.simulateTriggerClick();
        var spy = sinon.spy();
        this.$content.on("aui-dropdown2-item-check", spy);

        var $r1 = this.item('radio1-unchecked'),
            $r2 = this.item('radio2-checked'),
            $r3 = this.item('radio3-unchecked');

        this.simulateItemClick($r2);
        this.simulateItemClick($r1);
        this.simulateItemClick($r3);

        ok(spy.calledTwice, "Expected event listener called on hide");
    });

    test("Dropdown2 fires aui-dropdown2-item-uncheck on radios", function() {
        this.addRadioSection();
        this.simulateTriggerClick();
        var spy = sinon.spy();
        this.$content.on("aui-dropdown2-item-check", spy);

        var $r1 = this.item('radio1-unchecked'),
            $r2 = this.item('radio2-checked'),
            $r3 = this.item('radio3-unchecked');

        this.simulateItemClick($r1);
        this.simulateItemClick($r2);
        this.simulateItemClick($r3);

        ok(spy.calledThrice, "Expected event listener called on hide");
    });

    // Hide location

    test("Dropdown2 returned to original location if data-dropdown2-hide-location is not specified", function() {
        this.addPlainSection();
        var $hideParent = this.$content.parent()[0];

        this.simulateTriggerClick();
        this.clock.tick(100);
        this.simulateTriggerClick();
        this.clock.tick(100);

        ok(this.$content.parent()[0] === $hideParent, "Dropdown placed in original location");
        ok(this.$hideout.find(this.$content).length === 0, "Hideout does not contain dropdown");
    });

    test("Dropdown2 specifying the data-dropdown2-hide-location works properly when dropdown is hidden", function() {
        this.addPlainSection();
        this.$trigger.attr('data-dropdown2-hide-location', 'hideout');
        var $originalParent = this.$content.parent()[0];

        this.simulateTriggerClick();
        this.clock.tick(100);
        this.simulateTriggerClick();
        this.clock.tick(100);

        ok(this.$content.parent()[0] !== $originalParent, "Dropdown not placed in original location");
        ok(this.$hideout.find(this.$content).length === 1, "Hideout contains dropdown");
    });

    test("Dropdown2 in aui-dropdown2-trigger-group open on click correctly one dropdown", function() {
        this.addPlainSection();
        this.addPlainSection2();

        this.simulateTriggerClick();
        this.clock.tick(100);

        ok(this.triggerIsActive(), "Dropdown2 first is active");
        ok(!this.triggerIsActive(2), "Dropdown2 second is not active");
    });

    test("Dropdown2 in aui-dropdown2-trigger-group open on click correctly multiple dropdowns", function() {
        this.addPlainSection();
        this.addPlainSection2();

        this.simulateTriggerClick();
        this.clock.tick(100);
        this.simulateTriggerClick(2);
        this.clock.tick(100);

        ok(!this.triggerIsActive(), "Dropdown2 first is not active");
        ok(this.triggerIsActive(2), "Dropdown2 second is active");
    });

    test("Dropdown2 in aui-dropdown2-trigger-group open on hover after one is clicked", function() {
        this.addPlainSection();
        this.addPlainSection2();

        this.simulateTriggerClick();
        this.clock.tick(100);
        this.simulateTriggerHover(2);

        ok(!this.triggerIsActive(), "Dropdown2 first is not active");
        ok(this.triggerIsActive(2), "Dropdown2 second is active");
    });

    test("Dropdown2 in aui-dropdown2-trigger-group doesnt open on over when one not clicked", function() {
        this.addPlainSection();
        this.addPlainSection2();

        this.simulateTriggerHover(2);

        ok(!this.triggerIsActive(), "Dropdown2 first is not active");
        ok(!this.triggerIsActive(2), "Dropdown2 second is not active");
    });

    test("Dropdown2 in aui-dropdown2-trigger-group key navigation up down after one is clicked", function() {
        this.addPlainSection();
        this.addPlainSection2();

        this.simulateTriggerClick();
        this.clock.tick(100);

        this.pressDown();

        var $i1 = this.item('item1'),
            $i2 = this.item('item2');

        ok(this.triggerIsActive(), "Dropdown2 first is not active");
        ok(!$i1.is('.aui-dropdown2-active'), "Dropdown2 item 1 is not active");
        ok($i2.is('.aui-dropdown2-active'), "Dropdown2 item 2 is active");
    });

    test("Dropdown2 in aui-dropdown2-trigger-group key navigation left right after one is clicked", function() {
        this.addPlainSection();
        this.addPlainSection2();

        this.simulateTriggerClick();
        this.clock.tick(100);

        this.pressRight();
        this.clock.tick(100);
        this.pressDown();
        this.clock.tick(100);

        var $i12 = this.item('item12',2),
            $i22 = this.item('item22',2);

        ok(!this.triggerIsActive(), "Dropdown2 first is not active");
        ok(this.triggerIsActive(2), "Dropdown2 second is active");
        ok(!$i12.is('.aui-dropdown2-active'), "Dropdown2 second item 1 is not active");
        ok($i22.is('.aui-dropdown2-active'), "Dropdown2 second item 2 is active");
    });

    test("Dropdown2 adds aui-dropdown2-active and active to trigger on click", function() {
        this.addPlainSection();
        this.simulateTriggerClick();

        ok(this.$trigger.is('.aui-dropdown2-active'), "Dropdown2 has aui-dropdown2-active on click");
        ok(this.$trigger.is('.active'), "Dropdown2 has active on click");
    });

    test("Dropdown2 adds aui-dropdown2-checked on click checkboxes", function() {
        this.addCheckboxSection();
        this.simulateTriggerClick();

        var $c1 = this.item('check1-unchecked');

        this.simulateItemClick($c1);

        ok($c1.is('.checked.aui-dropdown2-checked'), "Dropdown2 checkbox 1 is checked");
    });

    test("Dropdown2 toggles checked to unchecked on checkboxes", function() {
        this.addCheckboxSection();
        this.simulateTriggerClick();

        var $c1 = this.item('check1-unchecked'),
            $c2 = this.item('check2-checked');

        this.simulateItemClick($c1);
        this.simulateItemClick($c2);

        ok($c1.is('.checked.aui-dropdown2-checked'), "Dropdown2 checkbox 1 is checked");
        ok(!$c2.is('.checked,.aui-dropdown2-checked'), "Dropdown2 checkbox 2 is unchecked");
    });

    test("Dropdown2 adds aui-dropdown2-checked on click radio buttons", function() {
        this.addRadioSection();
        this.simulateTriggerClick();

        var $r3 = this.item('radio3-unchecked');

        this.simulateItemClick($r3);

        ok($r3.is('.checked.aui-dropdown2-checked'), "Dropdown2 radio 3 is checked");
    });

    test("Dropdown2 toggles checked to unchecked on radio buttons", function() {
        this.addRadioSection();
        this.simulateTriggerClick();

        var $r1 = this.item('radio1-unchecked'),
            $r2 = this.item('radio2-checked');

        this.simulateItemClick($r1);

        ok($r1.is('.checked.aui-dropdown2-checked'), "Dropdown2 radio 1 is checked");
        ok(!$r2.is('.checked,.aui-dropdown2-checked'), "Dropdown2 radio 2 is unchecked");
    });

    test("Dropdown2 adds aui-dropdown2-disabled to hidden item - li.hidden > a", function() {
        this.addHiddenSection();
        this.simulateTriggerClick();

        var $h2 = this.item('hidden2-checked');

        ok($h2.is('.disabled.aui-dropdown2-disabled'), "Dropdown2 hidden item 2 is aui-dropdown2-disabled");
    });

    test("Dropdown2 adds aui-dropdown2-disabled to hidden and disabled item - li.hidden > a", function() {
        this.addHiddenSection();
        this.simulateTriggerClick();

        var $h1 = this.item('hidden1-unchecked-disabled');

        ok($h1.is('.disabled.aui-dropdown2-disabled'), "Dropdown2 hidden item 1 is aui-dropdown2-disabled");
    });

    test("Dropdown2 cannot click aui-dropdown2-disabled", function() {
        this.addHiddenSection();
        this.simulateTriggerClick();

        var $h1 = this.item('hidden1-unchecked-disabled'),
            $h2 = this.item('hidden2-checked');

        this.simulateItemClick($h1);
        this.simulateItemClick($h2);

        ok(!$h1.is('.checked.aui-dropdown2-checked'), "Dropdown2 hidden item 1 not checked when clicked");
        ok($h2.is('.aui-dropdown2-checked'), "Dropdown2 hidden item 2 not unchecked when clicked");
    });

    test("Dropdown2 aui-dropdown2-interactive doesnt hide on click", function() {
        this.addInteractiveSection();
        this.simulateTriggerClick();

        var $ir1 = this.item('iradio1-interactive-checked'),
            $ir2 = this.item('iradio2-interactive-unchecked');

        this.simulateItemClick($ir1);
        this.simulateItemClick($ir2);
        this.clock.tick(100);

        ok(this.triggerIsActive(), "Dropdown2 has aui-dropdown2-active on click");
    });

    test("Dropdown2 not aui-dropdown2-interactive hides on click", function() {
        this.addInteractiveSection();
        this.simulateTriggerClick();

        var $ir3 = this.item('iradio3-unchecked');

        this.simulateItemClick($ir3);
        this.clock.tick(100);

        ok(!this.triggerIsActive(), "Dropdown2 has aui-dropdown2-active on click");
    });
});